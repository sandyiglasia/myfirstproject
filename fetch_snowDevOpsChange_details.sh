#!/bin/bash

# Usage: ./fetch_from_servicenow.sh CHANGE_REQUEST_ID
# Example: ./fetch_from_servicenow.sh d733525f47da35108437f9cf016d4302

CHANGE_REQUEST_ID=$1
SERVICE_NOW_INSTANCE='https://dev183160.service-now.com'
USERNAME='admin'
PASSWORD='l9L5f@Mg@cFL'

# Fetch short_description from ServiceNow
response=$(curl -s -u "$USERNAME:$PASSWORD" "$SERVICE_NOW_INSTANCE/api/now/table/change_request/$CHANGE_REQUEST_ID?sysparm_display_value=true&sysparm_fields=number%2Cstate&sysparm_limit=1")

echo "$response"
